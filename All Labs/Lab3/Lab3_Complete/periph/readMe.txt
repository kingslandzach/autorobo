1) The "generalRobo" files are the same from Lab2.

2) The Left and Right wheels each have their own interrupts that are triggered when their respective timer counters reach a threshold set by the user

3) To travel a specific distance, a threshold set by the user is added to the running wheel timer count
	- For example if the Right Wheel has traveled 50 ticks and the user wants to 	travel 50 more, the Right Wheel will travel until it reaches 100 ticks.
4) The "makeRectangle()" function programs the robot to move both wheels 50 ticks forward, turn right, travel 100 ticks forward, turn right, travel 50 ticks forward, turn right, travel 100 ticks forward, and turn right to reach its original destination after traveling in a rectangle.

Note: Because of hardware differences found in the wheel's motors, different PWM values need to be used to ensure they travel at the same rate. These PWM adjustments can be seen in the "enablePWM()" function in the "generalRobo.c" file