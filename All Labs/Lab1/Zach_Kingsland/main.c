#include <stdint.h>
#include "inc/tm4c123gh6pm.h"

// PF3 is RGB (Green) = Pin31
/**
 * main.c
 */


#define GPIO_PORTF  ((volatile uint32_t *)0x40025000)
#define SYSCTL          ((volatile uint32_t *)0x400fe000)
enum {
   #define   GPIO_PIN_1              (1 << 2) // pin 1
   #define   GPIO_ALLPINS            0b11111111 // pins 0 to 7
     GPIO_DIR  =   (0x400 >> 2),
     GPIO_DEN  =   (0x51c >> 2),
   #define   SYSCTL_RCGCGPIO_PORTF (1 << 5)  // Port F
     SYSCTL_RCGCGPIO =       (0x608 >> 2),

   };
static void delay( void ) {
  for( uint32_t i = 0; i < 250000; ++i );  // requires C99 mode
}

int main(void)
{

    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF;
    GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_1;
    GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_1;
    while(1){
        GPIO_PORTF[GPIO_PIN_1] = 0;
        delay();
        GPIO_PORTF[GPIO_PIN_1] = GPIO_ALLPINS;
        delay();
    }
	return 0;
}
