1) The "periph" folder includes the "generalRobo" files from Lab2 and "interLED" files which configures two internal Tiva timers (T1CCP0 and T1CCP1) as two half-width timers which reach an interrupt state when their internal counter reaches a specified threshold. 

2) The "main.c" uses a combination of the "generalRobo" and "interLED" files to move the robot in a rectangle.

3) The "tm4c123gh6pm_startup_ccs.c" includes the individual wheel interrupts used when the timers' internal counters reach a specified threshold defined in the "makeRectangle()" function in "interLED.c".

4) The Left Wheel's Quadrature Encoder Interrupt is located on Port 'B' and the Right Wheel's interrupt is located on Port 'F'.
