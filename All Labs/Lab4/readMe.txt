1) The "generalRobo" files from Lab2 and the "bumper" files used to enable the bumper interrupts can be found in the "periph" folder.

2) The "main.c" is in a continuous while-loop until a bumper is pressed causing the robot to individually move the Right Wheel backwards when Bumper A is activated or move the Left Wheel bakcwards when Bumper B is activated, but will not perform both actions at the same time

3) The "main.c" responds to a flag set by the interrupt and resets the flag after performing the actions outlined in 2).

4) The "tm4c123gh6pm_startup_ccs.c" includes two bumper interrupts located at Ports "A" and "C" respectively 