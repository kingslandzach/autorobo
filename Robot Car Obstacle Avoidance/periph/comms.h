/*
 * comms.h
 *
 *  Created on: Apr 16, 2022
 *      Author: Zach
 */

#ifndef COMMS_H_
#define COMMS_H_

void enableUART(void);
uint32_t receiveData(void);
void sendData(uint8_t);
void disableUart(void);
void sendTestData(int16_t, int16_t, int16_t);
void sendTivaData(uint16_t, uint16_t, uint16_t);
void commTest(void);

#endif /* COMMS_H_ */
