/*
 * bump.c
 *
 *  Created on: Feb 22, 2022
 *      Author: Zach
 */
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/tm4c123gh6pm.h"
#include "generalRobo.h"

void bumpAInt(void){
        GPIOIntClear(GPIO_PORTA_BASE, GPIO_PIN_7);
        bumpAIntFlag = 1;
}
void bumpASys(void){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA));
     {
     }
}
void bumpAGPIO(void){
    GPIOIntRegister(GPIO_PORTA_BASE, bumpAInt);
    GPIOPinTypeGPIOInput(GPIO_PORTA_BASE, GPIO_PIN_7);
    GPIOIntTypeSet(GPIO_PORTA_BASE, GPIO_PIN_7, GPIO_RISING_EDGE);
    GPIOPadConfigSet(GPIO_PORTA_BASE, GPIO_PIN_7, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
    GPIOIntEnable(GPIO_PORTA_BASE, GPIO_PIN_7);
}
void bumpBInt(void){
    GPIOIntClear(GPIO_PORTF_BASE, GPIO_PIN_4);
    bumpBIntFlag = 1;
}
void bumpBSys(void){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF));
     {
     }
}
void bumpBGPIO(void){
    GPIOIntRegister(GPIO_PORTF_BASE, bumpBInt);
    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);
    GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_RISING_EDGE);
    GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);
    GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4);
}
void bumpAAll(void){
    bumpASys();
    bumpAGPIO();
}
void bumpBAll(void){
    bumpBSys();
    bumpBGPIO();
}
void bumpAll(void){
    bumpAAll();
    bumpBAll();
}
