#include <stdint.h>
#include <stdbool.h>
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/pin_map.h"
#include "inc/tm4c123gh6pm.h"
//#include "initpwm.h"
//#include "motor.h"
//#include "initgpio.h"
/*
 * interrupt.h
 *
 *  Created on: Feb 16, 2022
 *      Author: Devin Garcia
 */

#ifndef INTERRUPT_H_
#define INTERRUPT_H_
extern int flagL;
extern int flagR;
void intRWheel(void);
void intLWheel(void);
void enableTimer(void);
void setWheelMatch(int,int);
void forwardX(int);
void backwardX(int);
void turnRX(int);
void turnLX(int);
void makeRectangle(void);

#endif /* INTERRUPT_H_ */

