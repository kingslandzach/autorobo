/*
 * bumper.h
 *
 *  Created on: Feb 22, 2022
 *      Author: Zach
 */

#ifndef BUMPER_H_
#define BUMPER_H_
void bumpAInt(void);
void bumpASys(void);
void bumpAGPIO(void);
void bumpAAll(void);
void bumpBInt(void);
void bumpBSys(void);
void bumpBGPIO(void);
void bumpBAll(void);
void bumpInt(void);
void bumpSys(void);
void bumpGPIO(void);
void bumpAll(void);
extern int bumpAIntFlag;
extern int bumpBIntFlag;


#endif /* BUMPER_H_ */
