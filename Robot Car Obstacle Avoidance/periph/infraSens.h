/*
 * infraSens.h
 *
 *  Created on: Mar 20, 2022
 *      Author: Zach
 */

#ifndef INFRASENS_H_
#define INFRASENS_H_

void enableSens(void);
uint32_t readSens1(void);
uint32_t readSens2(void);
uint32_t readSens3(void);
#endif /* INFRASENS_H_ */
