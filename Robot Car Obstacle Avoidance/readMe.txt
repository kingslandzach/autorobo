Control Methods (Found in "Raspberry Pi Files":
	- Communication Protocol (Found in "writeTest.py" in "Raspberry Pi Files"
		- 2 Bytes: (2-bits) (3-bits) (11-bits)
		- (Type) (Specific) (Data)
		- Type: 'move' or 'scan'
		- Specific: 
			a) 'move': forward, backward, turnL, turnR
			b) 'scan': 1, 2, 3
				i) 1 = Forward
				ii) 2 = Right
				iii) 3 = Left
		- Data:
			a) 'move': The data sent is the number of ticks the robot should travel, the data received is the number of ticks the robot did travel
			b) 'scan': The data sent is irrelevant, the data received is the IR sensor reading
		Note: This protocol is full fledged in 'writeTest.py' and the commands in 'bug2.py' are derived from 'writeTest.py'

	- Obstacle Avoidance
		- Bug2 Based (Found in "bug2.py" in "Raspberry Pi Files")
		1. Idle State
			1) Scan Forward IR 
			2) If object is detected, go to step 4
			3) If object isn't detected, move forward 1, and go back to step 1
			4) Turn 90 degrees left, go to step 1 in "Trace State"
		2. Trace State
			1) Scan Forward IR
			2) If object is detected, go to step 4 in "Idle State"
			3) If object isn't detected, move forward 1, and go to step 4
			4) Scan Right IR
			5) If object is detected, go to step 1
			6) If object isn't detected, go to step 1 in "Turn Right State"
				a) Also initialize a counter to 0
		3. Turn Right State
			1) Scan Forward IR
			2) If object is detected go to step 4 in "Idle State"
			3) If object isn't detected, move forward 1, and go to step 4
			4) Scan Right IR
			5) If object is detected, go to step 1 in "Trace State"
			6) If object is not detected, add 1 to count, and go to step 7
			7) If count is greater than or equal to 25: go to step 8, else: go to step 1
			8) Turn 90 degrees right, go to step 1 of "Idle State"
		4. Bump State (Interrupt State)
			1) If bumpA is touched: backup and turn right, go to step 3
			2) If bumpB is touched: backup and turn left, go to step 3 
		 	3) Return to previous state and step
		Note: The counter initialized in step 6 of "Trace State" is used to ensure the robot can move forward enough before turning to clear the edge of the obstacle it is tracing. This counter is incremented by one if each wheel moves forward one tick and is reset once the robot turns left due to another obstacle or turns right due to the ticks traveled fulfilling the threshold of 25.
	 
	- Path Planning + Map Creation (Found in "dijkstra.py" in "Raspberry Pi Files")	
		- Dijkstra Based
		Note: Map is initialized using 0s as free space, 1s as obstacles, 2 as the start position, 3 as the goal position, and 8 as the current path of previous positions
		1) The algorithm starts at the goal with direction pointing to the right
		2) The algorithm checks up, down, left, and right for the goal
			a) If goal: Go to goal and go to Step 5
			b) Else: Step 3
		3) The algorithm checks the direction it is pointing for free space, starting location, or obstacle
			a) If free space: Go to position and go to Step 4
			b) Else: Rotate direction 90 degrees clockwise (right -> down -> left -> up -> right) and repeat Step 3
		4) Add previous position to path and update current position and current direction
		5) Add path to pathList and repeat steps 1-4 4 times (1 time for each different starting position)
			a) After 4 paths are added, go to Step 6
		6) Print the lengths of the 4 paths created

		Whats missing: 
			- Localization based on Map
			- Optimal path planning algorithm (algorithm currently finds 4 extremely poor paths)

Demonstration:
	- Expected Outcome:
		- Bug2 steps to be fully implemented.
		- The robot should move forward until it meets an obstacle, turn left, and repeat until it reaches the goal (where it would be manually turned off)		
	- Problems:
		- Because of noise, the robot doesn't go forward and then turn left, but randomly bumps into objects or detects obstacles that it wouldn't see if it just went straight.
		- The robot doesn't have a way to see the end goal
	- Solution:
		- There were solutions outlined in the past that could solve the noise issue which would easily solve the problem
		- Another solution could be localization and utilizing multiple positions on the map based on noise. 
		- Additionally, if a camera were to be implemented, it would be easy to see the goal, allowing the robot to know when it is finished moving.

	
	Without the implementation of the of the map:
		- The robot made use of an attempted version of Bug 2 algorithm for obstacle avoidance 
		- The robot would be able to detect obstacles via the IR sensors and make left turns upon seeing the objects 
		through the front IR sensors 
		- The Bump sensors would cause the robot to move away from the obstacles upon making contact
		- The robot would not be able to detect the goal of the obstacle course and requires manual shutdown 

Future Implementation:
	Building off of current implementation
	- The robot's obstacle avoidance core ideals seems to work great. I would love to implement some kind of noise correction and localization based on noise since the robot drifts more and more as time goes on. 
	- Additionally, the map was completed, but the path-planning wasn't fully implemented. 
	- If done again, or if there were more time I would develop dijkstra's or a similar algorithm to plan the path. Localization also needs to be implemented fully. 
	
	Going a different direction
	- Image processing is something I didn't get to experiment with in this course or in Senior Project. 
	- Using the cameras can be buggy, but I would've liked to at least indicate when the robot reaches the goal using the camera.
	
	Fully implemented Dijkstra's path planning: 
		- original code of the path planning method would be able to create a path within the map until it finds the starting position
		- the path would be unable to find the shortest and most optimal path within the map 
		- the code requires a method to store the locations of the path to the starting position (preferably varying paths) 
			- based on the stored paths compare each to find the optimal and shortest path 
				- the optimal path would be based on the least number of turns required to reach the goal from the starting position 
				- the shortest path would be the least number of ticks that the robot has to move to reach the goal from the starting position
		- the code for the path planning should also account for the direction that the robot is facing before attempting to make the path
			- the orientation of the robot in the graph would have to be initialized before creating the path 
				- relative to the robot's orientation; the robot must either go left, up, down, or right when creating the path to the starting position 
					of the map

Side Notes (If you want to hear my inner thoughts):
	- I came into this course knowing it was going to be the hardest class I could possible choose to take, mainly because I barely (and really shouldn't have) passed microcontrollers. At the beginning, I really didn't know what I was doing. The questions I asked Jason at the beginning were embarassing. However, the more work I put in, the more rewarding it became, and I realized that large scale projects like this are what I dream about. 
	- With my success in Senior Project and growth in Robotics, I'm sorry I didn't seek you out sooner in my college career. Maybe things would've turned out differently for me.
	- Thank you for teaching me and thank you for trusting me enough to give me 4 Raspberry Pi 4s (which each cost ~$150) at 7 PM on a Friday,
		Zach
