#include <stdint.h>
#include <stdbool.h>
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/pin_map.h"
#include "inc/tm4c123gh6pm.h"
#include "inc/hw_ints.h"
#include "interLED.h"
#include "driverlib/interrupt.h"
#include "generalRobo.h"
#include "driverlib/timer.h"
//#include <interruptLEDs.h>
//#include "initpwm.h"
//#include "motor.h"
//#include "initgpio.h"
#define pi 3.14592625359
#define r 3 // cm
int flagL = 0;
int flagR = 0;
int wheelRCount = 0;
int wheelLCount = 0;
double tickDist = (1/32) * 2 * pi * r; // in cm
enum{
    GPTMTAV = 0X050,
    GPTMTBV = 0X054,
};
void intRWheel(void){
    TimerIntClear(TIMER1_BASE, TIMER_CAPB_MATCH);
    flagR = 1;
}
void intLWheel(void){
    TimerIntClear(TIMER1_BASE, TIMER_CAPA_MATCH);
    flagL = 1;
}

void enableTimer(void){
   // GPIOPinConfigure(GPIO_PF0_T0CCP0);
   // GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_0);
   // GPIOPinTypeTimer(GPIO_PORTF_BASE,GPIO_PIN_0 );
    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER1))
    {
    }
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF))
    {
    }
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB))
    {
    }
    GPIOPinTypeTimer(GPIO_PORTB_BASE, GPIO_PIN_4);
    GPIOPinTypeTimer(GPIO_PORTF_BASE, GPIO_PIN_3);
    GPIOPinConfigure(GPIO_PB4_T1CCP0);
    GPIOPinConfigure(GPIO_PF3_T1CCP1);
    GPIOIntRegister(GPIO_PORTB_BASE, intLWheel);
    GPIOIntRegister(GPIO_PORTF_BASE, intRWheel);


    TimerConfigure(TIMER1_BASE, (TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_CAP_COUNT_UP | TIMER_CFG_B_CAP_COUNT_UP));
    TimerLoadSet(TIMER1_BASE,TIMER_BOTH, 0);
    TimerPrescaleSet(TIMER1_BASE, TIMER_BOTH, 2);
    TimerControlEvent(TIMER1_BASE, TIMER_BOTH, TIMER_EVENT_POS_EDGE);
    TimerMatchSet(TIMER1_BASE, TIMER_BOTH, 1);
    TimerEnable(TIMER1_BASE, TIMER_BOTH);
    TimerIntRegister(TIMER1_BASE, TIMER_A, intLWheel);
    TimerIntRegister(TIMER1_BASE, TIMER_B, intRWheel);
    TimerIntEnable(TIMER1_BASE, TIMER_CAPA_MATCH |TIMER_CAPB_MATCH);

    IntRegister(INT_TIMER1A, intLWheel);
    IntRegister(INT_TIMER1B, intRWheel);
    IntEnable(INT_TIMER1A);
    IntEnable(INT_TIMER1B);

    IntMasterEnable();
}
void setWheelMatch(int matchA, int matchB){
    matchA = TimerValueGet(TIMER1_BASE, TIMER_A) + matchA;
    matchB = TimerValueGet(TIMER1_BASE, TIMER_B) + matchB;
    TimerMatchSet(TIMER1_BASE,TIMER_A, matchA); //sets match for L Wheel
    TimerMatchSet(TIMER1_BASE,TIMER_B, matchB); //sets match for R Wheel
}
void forwardX(int x){
    setWheelMatch(x,x);
    wheelsForward();
    while(1){
        if(flagL == 1 )
            wheelLStop();
        if(flagR == 1)
            wheelRStop();
        if(flagL == 1 && flagR == 1)
            break;
    }
    wheelsStop();
    flagL = 0;
    flagR = 0;
}
void backwardX(int x){
    setWheelMatch(x,x);
    wheelsBackward();
    while(1){
        if(flagL == 1 )
            wheelLStop();
        if(flagR == 1)
            wheelRStop();
        if(flagL == 1 && flagR == 1)
            break;
    }
    wheelsStop();
    flagL = 0;
    flagR = 0;
}
void turnRX(int x){
    setWheelMatch(x, x);
    wheelsRTurn();
    while(1){
       if(flagL == 1 )
           wheelLStop();
       if(flagR == 1)
           wheelRStop();
       if(flagL == 1 && flagR == 1)
           break;
    }
    wheelsStop();
    flagL = 0;
    flagR = 0;
}
void turnLX(int x){
    setWheelMatch(x, x);
    wheelsLTurn();
    while(1){
       if(flagL == 1 )
           wheelLStop();
       if(flagR == 1)
           wheelRStop();
       if(flagL == 1 && flagR == 1)
           break;
    }
    wheelsStop();
    flagL = 0;
    flagR = 0;
}
void makeRectangle(void){
   for(int i = 0; i < 2; i++){
       forwardX(100);
       delay10();
       turnRX(12);
       delay10();
       forwardX(50);
       delay10();
       turnRX(12);
       delay10();
   }
   wheelsStop();
}


