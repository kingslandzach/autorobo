#include <stdint.h>
#include <stdbool.h>
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/pin_map.h"
#include "inc/tm4c123gh6pm.h"
#include "generalRobo.h"

void main(void) 
{
	enableAll();
	wheelsForward();
	delay10();
	wheelsBackward();
	delay10();
	wheelsStop();
}