#include <stdint.h>
#include <stdbool.h>
/*
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/uart.h"

#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/tm4c123gh6pm.h"
*/
#include "generalRobo.h"
#include "infraSens.h"
#include "bumper.h"
#include "interLED.h"
#include "comms.h"


//UI TEST SEND


/**
 * main.c
 */
int main(void)
{
    enableWheels();
    enableSens();
    enableTimer();
    enableBumpers();
    enableUART();
    //initUART();
    /*while(1){
        UiTest();
    }*/
    //sendTestData(0,0,0);
    while(1){
       commTest();
    }
    //commTest();

    //disableUart();

    return 0;
}



/*
extern int bumpAIntFlag;
extern int bumpBIntFlag;
void main()
{
        uint32_t ifsValue;
        enableWheels();
        enableTimer();
        enableBumpers();
        enableSens();

        while(1){
            delay10();
            ifsValue = readSens();
            if(bumpAIntFlag || bumpBIntFlag){
                if(bumpAIntFlag == 1){
                    wheelRBackward();
                    delay10();
                    wheelRStop();
                    delay10();
                    bumpAIntFlag = 0;
                }
                if(bumpBIntFlag == 1){
                    wheelLForward();
                    delay10();
                    wheelLStop();
                    delay10();
                    bumpBIntFlag=0;
                }
            }
            else if(ifsValue > 800){
                wheelsBackward();
                delay10();
                wheelRStop();
                wheelLBackward();
                delay10();
                wheelLStop();
            }
            else{
                wheelsForward();
            }
        }
        return;
}
*/

