#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/tm4c123gh6pm.h"
#include "generalRobo.h"
#include "driverlib/adc.h"

#define GPIO_PORTE          ((volatile uint32_t *) 0x40024000)
#define ADC0                ((volatile unint32_t *) 0x40038000)
enum {
#define PIN_1               (1 << 1)
       GPIO_AFSEL = (0x420 >> 1),
       ADCSSMUX0 = (0x040 >> 1)
};

void enableSens(void){
    // Enable the ADC0 module
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);

    // Wait for the ADCO module to be ready
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0))
    {
    }
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE))
        {
        }
    GPIOPinTypeADC(GPIO_PORTE_BASE, (GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3));
    //Configure the Alternate functions PE1
    GPIO_PORTE[GPIO_AFSEL] |= 1;
    GPIO_PORTE[ADCSSMUX0]  |= 2;
    //Configure the Alternate functions PE2
    GPIO_PORTE[GPIO_AFSEL] |= 2;
    GPIO_PORTE[ADCSSMUX0]  |= 1;
    //Configure the Alternate functions PE3
    GPIO_PORTE[GPIO_AFSEL] |= 3;
    GPIO_PORTE[ADCSSMUX0]  |= 0;

    // Enable the first sample sequencer to capture the value of channel 0 when
    // the processor trigger occurs.
    //AIN2 PE1
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0,
    ADC_CTL_IE | ADC_CTL_END | ADC_CTL_CH2);
    ADCSequenceEnable(ADC0_BASE, 1);
    //AIN1 PE2
    ADCSequenceConfigure(ADC0_BASE, 2, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 2, 0,
    ADC_CTL_IE | ADC_CTL_END | ADC_CTL_CH1);
    ADCSequenceEnable(ADC0_BASE, 2);
     //AIN0 PE3
    ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 3, 0,
    ADC_CTL_IE | ADC_CTL_END | ADC_CTL_CH0);
    ADCSequenceEnable(ADC0_BASE, 3);

}
uint32_t readSens1(void){
    //Review PWMs to make use of the proper GPIO Pins
    // Enable Port E Pin 1 which has the AIN Signal
    uint32_t readADCvalue;
    ADCProcessorTrigger(ADC0_BASE, 1);
    while(!ADCIntStatus(ADC0_BASE, 1, false))
    {
    }
    ADCSequenceDataGet(ADC0_BASE, 1, &readADCvalue);
    return readADCvalue;
}
uint32_t readSens2(void){
    //Review PWMs to make use of the proper GPIO Pins
    // Enable Port E Pin 1 which has the AIN Signal
    uint32_t readADCvalue;
    ADCProcessorTrigger(ADC0_BASE, 2);
    while(!ADCIntStatus(ADC0_BASE, 2, false))
    {
    }
    ADCSequenceDataGet(ADC0_BASE, 2, &readADCvalue);
    return readADCvalue;
}
uint32_t readSens3(void){
    //Review PWMs to make use of the proper GPIO Pins
    // Enable Port E Pin 1 which has the AIN Signal
    uint32_t readADCvalue;
    ADCProcessorTrigger(ADC0_BASE, 3);
    while(!ADCIntStatus(ADC0_BASE, 3, false))
    {
    }
    ADCSequenceDataGet(ADC0_BASE, 3, &readADCvalue);
    return readADCvalue;
}
