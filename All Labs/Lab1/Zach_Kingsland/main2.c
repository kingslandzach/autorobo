#include <stdint.h>
#include <stdbool.h>
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/tm4c123gh6pm.h"

static void delay(void){
    for (uint32_t i = 0; i<250000; ++i); //delay for LED
}

int main(void){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF))
    {
    }
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_2);

    while(1){
         //turn light off = 0
        GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2,0);
         delay();
         //turn light on = 1
         GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, GPIO_PIN_2);
         delay();
     }


    return 0;
}
