#include <stdint.h>
#include <stdbool.h>
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/uart.h"

#include "driverlib/sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/tm4c123gh6pm.h"

#include "generalRobo.h"
#include "infraSens.h"
#include "interLED.h"


void enableUART(void){
    //Enable Port B
        SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
           while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB))
               {
               }

        //
        // Enable the UART1 module.
        //
        SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);
        //
        // Wait for the UART1 module to be ready.
        //
        while(!SysCtlPeripheralReady(SYSCTL_PERIPH_UART1))
        {
        }
        GPIOPinConfigure(GPIO_PB0_U1RX);
        GPIOPinConfigure(GPIO_PB1_U1TX);
        GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);

        // Initialize the UART. Set the baud rate, number of data bits, turn off
        // parity, number of stop bits, and stick mode. The UART is enabled by the July 25, 2016 579 UART
        // function call.
        //
        UARTConfigSetExpClk(UART1_BASE, SysCtlClockGet(), 9600, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

        //Enable
        UARTEnable(UART1_BASE);

        //Enable the FIFO FUNCTION
        UARTFIFOEnable(UART1_BASE);
}

uint32_t receiveData(void){
    //Function call to wait until a character is received before returning FUNCTION
      return UARTCharGet(UART1_BASE);
}

void sendData(uint8_t binary){
    // Put a character in the output buffer.
      //
    UARTCharPut(UART1_BASE, binary);

}


void disableUart(void){
        // Disable the UART.
        //
        UARTDisable(UART1_BASE);
}

void sendTestData(int16_t cat, int16_t spec, int16_t data){
    //Put packet together
    while(1){
          sendData(0b0000000000000001);
          delay();
      }
}

void sendTivaData(uint16_t cat, uint16_t spec, uint16_t data){ //SEND DATA MODIFY
    // 2 bits, 3 bits, 11 bits
    cat = cat << 14;
    spec = spec << 11;
    data = data << 5;
    data = data >> 5;
    uint16_t tivaData = cat + spec + data;
    uint8_t tivaData1 = (tivaData << 8) >> 8;
    uint8_t tivaData2 = tivaData >> 8;
    sendData(tivaData2);
    sendData(tivaData1);
    delay1();

}

void commTest(void){ //Receiving
    uint16_t piData1 = receiveData(); //Receive 1st 8 bits
    uint16_t piData2 = receiveData(); //Receive 2nd 8 bits

    piData1 = piData1 << 8; //Shift the first bit 8 to the left for scale
    uint16_t piData = piData1 + piData2; //Add the 2 8 bit values together to make the complete packet
    uint16_t cat = piData >> 14; //2 bits: move, scanIR, mode
    uint16_t spec = piData << 2; //3 bits: specific function
    spec = spec >> 13;
    uint16_t data = piData << 5; //11 bits: data or measurement
    data = data >> 5;
    uint32_t tivaData;
    switch(cat){
        // 0b0000 0000
        case 0b00: //move
            switch(spec){
                case 0b000: //stop
                    wheelsStop();
                    sendTivaData(cat, spec, data);
                break;
                case 0b001: //forward
                    data = forwardX(data);
                    sendTivaData(cat, spec, data);
                break;
                case 0b010: //reverse
                    data = backwardX(data);
                    sendTivaData(cat, spec, data);
                break;
                case 0b011: // turn L
                    data = turnLX(data);
                    sendTivaData(cat, spec, data);
                break;
                case 0b100: //turn R
                    data = turnRX(data);
                    sendTivaData(cat, spec, data);
                break;
                default:
                    sendTivaData(cat, spec, data);
                break;
            }
        break;
        case 0b01: //scan IR
            switch(spec){
                case 0b111: //scan all IR
                    tivaData = readSens1();
                    sendTivaData(cat, spec, tivaData);
                break;
                case 0b001: //forward infrared (1)
                    tivaData = readSens1();
                    sendTivaData(cat, spec, tivaData);
                break;
                case 0b010: //right infrared (2)
                    tivaData = readSens2();
                    sendTivaData(cat, spec, tivaData);
                break;
                case 0b100: //left infrared (3)
                    tivaData = readSens3();
                    sendTivaData(cat, spec, tivaData);
                break;
                default: //nop
                    sendTivaData(cat, spec, 0b111111111111);
            }
        break;

        case 0b11: // mode
            switch(spec){
                case 0b001: // Obstacle Avoidance on
                    sendTivaData(cat, spec, 0b0000000000000001);
                break;

                default: //off
                    sendTivaData(cat, spec, 0b0000000000000000);
            }
        break;

        default:
            sendTivaData(cat, spec, 0b0000000000000000);
        break;

    }// end of cat case


}//end of function
