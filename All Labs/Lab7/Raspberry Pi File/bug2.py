import struct
import time
import serial

port = serial.Serial('/dev/ttyS0', baudrate=115200)
obstacleAvoid = 1
#while obstacleAvoid == 0:
#	flag = 0
#	while flag < 2:
#		flag = 0
#		catBits = 0
#		specBits = 0
#		sendData = 0
#		byte1 = 0
#		byte2 = 0
#		#gather user input
#		cat = input("Category: ")
#		spec  = input("Specific: ")
#		data = input("Data: ")
#		#data cannot be larger than 2^11
#		if int(data) > 2047:
#			data = 2047
#		#all move commands
#		if cat ==  "move":
#			catBits = 0b00
#			sendData = 0b0000000000000000
#			flag += 1
#			if spec == 'stop':
#				specBits = 0b000
#				sendData += 0b0000000000000000
#				flag += 1
#			elif spec == 'forward':
#				specBits = 0b001
#				sendData += 0b0000100000000000
#				flag += 1
#			elif spec == 'backward':
#				specBits = 0b010
#				sendData += 0b0001000000000000
#				flag += 1
#			elif spec == 'turnL':
#				specBits = 0b011
#				sendData += 0b0001100000000000
#				flag += 1
#			elif spec == 'turnR':
#				specBits = 0b100
#				sendData += 0b0010000000000000
#				flag += 1
#			sendData += int(data)
#		#all scan commands
#		elif cat == "scan":
#			catBits = 0b01
#			sendData = 0b0100000000000000
#			flag += 1
#			if spec == 'all':
#				specBits = 0b111
#				sendData += 0b0011100000000000
#				flag += 1
#			elif spec == '1':
#				specBits = 0b001
#				sendData += 0b0000100000000000
#				flag += 1
#			elif spec == '2':
#				specBits = 0b010
#				sendData += 0b0001000000000000
#				flag += 1
#			elif spec == '3':
#				specBits = 0b100
#				sendData += 0b0010000000000000
#				flag += 1
#			elif spec == '4':
#				specBits = 0b101
#				sendData += 0b0010100000000000
#				flag += 1
#		#all mode commands
#		elif cat == "mode":
#			catBits = 0b11
#			sendData = 0b1100000000000000
#			flag += 1
#			if spec == "on":
#				specBits = 0b001
#				sendData += 0b0000100000000000
#				flag += 1
#				obstacleAvoid = 1
#			elif spec == "off":
#				specBits = 0b000
#				sendData += 0b0000000000000000
#				flag += 1
#		if flag < 2:
#			print("Try again\n")
#	print(sendData)
#	#split 2 bytes in half
#	sendData = sendData.to_bytes(2,'big')
#	print(sendData)
#	#request a packet and unpack it
#	packed = struct.pack('BB', sendData[0], sendData[1])
#	port.write(packed)
#	if catBits == 0b11 and specBits == 0b111:
#		readDataList = []
#		for i in range(0,3):
#			readData1 = b''
#			readData2 = b''
#			while readData1 == b'':
#				readData1 = port.read()
#				readData2 = port.read()
#			readDataList.append(readData1 + readData2)
#			print(readData)
#		#read back 4 16-bit (2-byte) data from tiva
#	else:
#		readData1 = b''
#		readData2 = b''
#		readData = b''
#		while True:
#			if(port.in_waiting > 0):
#				readData1 = port.read()
#				readData2 = port.read()
#				break
#		readData = readData1 + readData2
#		readDataCat = int.from_bytes(readData, 'big') >> 14
#		readDataSpec = (int.from_bytes(readData,'big') >> 11) - (readDataCat << 3)
#		readDataData = (int.from_bytes(readData, 'big')) - (readDataSpec << 11) - (readDataCat << 14)
#		#read back 1 16-bit (2-byte) data from tiva
#	#sendData = int.from_bytes(sendData, 'big') >> 11
#	#readData1 = int.from_bytes(readData1, 'little')
#	#readData2 = int.from_bytes(readData2, 'little')
#	print('Original Message: ' + str(sendData))
#	print('Tiva Message: ' + str(readData))
#	print(readDataCat)
#	print(readDataSpec)
#	print(readDataData)


r = 3
pi = 3.14592625359
tickDist = (1/32) * 2 * pi * r
data = 0
step = 0
while obstacleAvoid == 1:
	#forward threshold should be 1000
	#Right threshold should be 1200
	#left threshold should be 1100
	#Forward = 00 001 00000000000
	#turnL: = 00 011 00000000000
	#scan forward = 01 001
	#scan right = 01 010
	if step == 0: #default
		#Scan forward
		sendData = 0b0100100000000000
	elif step == 1:
		#if no obstacle: go towards goal
		#For now, wheels forward
		#forward 1:
		sendData = 0b0000100000000001
	elif step == 2:
		#if obstacle: turn 90 degrees left
		#turnL 14:
		sendData = 0b0001100000001100
	elif step == 3:
		#stop state
		#stop 0
		sendData = 0b0000000000000000
	elif step == 4:
		sendData = 0b0101000000000000
	elif step == 5:
		#if object is still seen on right side
		#forward 1
		sendData = 0b0000100000000001
	elif step == 6:
		#if the object has successfully been cleared
		#turnR 14:
		sendData = 0b0010000000001100
	#Forward 1:
	#sendData = 0b0000100000000001

	sendData = sendData.to_bytes(2,'big')
	packed = struct.pack('BB', sendData[0], sendData[1])
	port.write(packed)
	while True:
		if(port.in_waiting > 0):
			readData1 = port.read()
			readData2 = port.read()
			break
	readData = readData1 + readData2
	if step == 0 or step == 4:
		data = int.from_bytes(readData, 'big') - int.from_bytes(sendData, 'big')
	else:
		data = 0
	print(sendData)
	print(readData)
	print(data)
	if step == 0:
		if data < 1200:
			step = 1
		else:
			step = 2
	elif step == 2:
		step = 3
	elif step == 3:
		step = 4
	elif step == 4:
		if data > 1000:
			step = 5
		else:
			step = 6
	elif step == 5:
		step = 4
	else:
		step = 0

print('out')
port.close()
