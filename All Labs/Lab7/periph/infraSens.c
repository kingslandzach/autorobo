#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/tm4c123gh6pm.h"
#include "generalRobo.h"
#include "driverlib/adc.h"

#define GPIO_PORTE          ((volatile uint32_t *) 0x40024000)
#define ADC0                ((volatile unint32_t *) 0x40038000)
enum {
#define PIN_1               (1 << 1)
       GPIO_AFSEL = (0x420 >> 1),
       ADCSSMUX0 = (0x040 >> 1)
};

void enableSens(void){
    // Enable the ADC0 module
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);

    // Wait for the ADCO module to be ready
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_ADC0))
    {
    }
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOE))
        {
        }
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_1);
    //Configure the Alternate functions
    GPIO_PORTE[GPIO_AFSEL] |= 1;
    GPIO_PORTE[ADCSSMUX0]  |= 2;
    //
    // Enable the first sample sequencer to capture the value of channel 0 when
    // the processor trigger occurs.
    //
    ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 1, 0,
    ADC_CTL_IE | ADC_CTL_END | ADC_CTL_CH2);
    ADCSequenceEnable(ADC0_BASE, 1);
}
uint32_t readSens(void){
    //Review PWMs to make use of the proper GPIO Pins
    // Enable Port E Pin 1 which has the AIN Signal
    uint32_t readADCvalue;
    ADCProcessorTrigger(ADC0_BASE, 1);
    while(!ADCIntStatus(ADC0_BASE, 1, false))
    {
    }
    ADCSequenceDataGet(ADC0_BASE, 1, &readADCvalue);
    return readADCvalue;
}
