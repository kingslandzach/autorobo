import struct
import serial

r = 3
pi = 3.149262359
tickDist = (1/32) * 2 * pi * r

count = 0
data = 0
step = 0
state= 0

port = serial.Serial('/dev/ttyS0', baudrate=9600)
while True:
	#
	#Obstacle Avoidance Steps
	#
	#Scan in front
	if step == 0: #Scan Forward (1)
		sendData = 0b0100100000000000
		baseData = 0b0100100000000000
	elif step == 1: #move forward 1
		sendData = 0b0000100000000001
		baseData = 0b0000100000000000
	elif step == 2: #left turn 12
		sendData = 0b0001100000001100
		baseData = 0b0001100000000000
	elif step == 3: #Scan Right
		sendData = 0b0101000000000000
		baseData = 0b0101000000000000
	elif step == 4: #Turn Right
		sendData = 0b0010000000001100
	else:
		break
	#Convert Send Data from integer to bytes
	sendData = sendData.to_bytes(2,'big')
	#Create packet to send
	packet = struct.pack('BB', sendData[0], sendData[1])
	#Write the packet to the Tiva
	port.write(packet)
	#Read response from Tiva
	readData1 = port.read()
	readData2 = port.read()
	readData = readData1 + readData2
	#Extract the data
	data = int.from_bytes(readData,'big') - baseData
	#Continue to scan until an obstacle is detected by IR Sensor 1 (Forward)
	if state == 0: #Go Forward until Obstacle
		count = 0
		if step == 0: #Scan Forward
			if data < 1200: #No Obstacle
				step = 1
			else: #Obstacle
				step = 2
		elif step == 1: #Move Forward
			step = 0
		elif step == 2: #Turn Left
			state = 1
			step = 0
		else:
			step = 0
	elif state == 1: #Move around obstacle
		if step == 0: #Scan Forward
			if data < 1200: #No Obstacle
				step = 1
			else: #Obstacle
				step = 2
		elif step == 1: #Move Forward
			step = 3
		elif step == 2: #Turn Left
			step = 0
		elif step == 3: #Scan Right
			if data < 1000: #No Obstacle
				state = 2
				step = 0
			else: #Obstacle
				step = 0
		else:
			step = 0
	elif state == 2: #Move until clear of object and turn right
		if step == 0: #Scan Forward
			if data < 1200:
				step = 1
			else:
				step = 2
				state = 1 #Reset to state 1
		elif step == 1: #Move Forward
			step = 3
		elif step == 3: #Scan Right
			if data < 1000:
				if count < 25:
					count += 1
					step = 0
				else:
					step = 4
			else:
				step = 0
				state = 1
		elif step == 4: #Turn Right
			state = 0
			step = 0 
		else:
			print('Error')
			break
		#Check if end has been reached based on number of right turns
	#Print send and receive packets along with the resulting data extracted
	print(sendData)
	print(readData)
	print(data)
