import numpy as np

map = np.zeros((31,16))

#Initialize Map
for i in range(31):
	map[i][0]=1
for i in range(16):
	map[0][i]=1
	map[30][i]=1
for i in range(28):
	map[i][15]=1
col = 10
for col in range(10,16):
	map[28][col]=1
row2 = 20
col2 = 10
for row2 in range(20,29):
	map[row2][col2]=1
row3 = 20
col3 = 5
for col3 in range(5,11):
	map[row3][col3]=1
row4 = 13
col4 = 5
for row4 in range(13,21):
	map[row4][col4]=1
row5 = 13
col5 = 5
for col5 in range(5,9):
	map[row5][col5]=1
row6 = 10
col6 = 8
for row6 in range(10,14):
	map[row6][col6]=1
row7 = 10
col7 = 3
for col7 in range(3,9):
	map[row7][col7]=1
row8 =  10
col8 = 3
for row8 in range(10,22):
	map[row8][col8]=1
row9 = 21
col9 = 3
for col9 in range(3,9):
	map[row9][col9]=1
row10 = 21
col10 = 9
for row10 in range(21,26):
	map[row10][col10]=1
for i in range(10):
	map[25][i]=1
row11 = 5
col11 = 5
for col11 in range(5,11):
	map[row11][col11]=1
row12 = 6
col12 = 5
for col12 in range(5,11):
	map[row12][col12]=1
map[26][12] = 2
map[23][7] = 3  
#print(map)
#Define all variables
#Define Start and End Points
start = (26,12)
goal = (23,7)
currDir = ''
nextDir = ''
pathList = []
foundFlag = 0
upFlag = 0
riFlag = 0
leFlag = 0
doFlag = 0
origMap = map.copy()

upPos = (0,0)
riPos = (0,0)
doPos = (0,0)
lePos = (0,0)
#Search for best path out of 4 starting directions
for i in range(4):
	if currDir == 'right':
		currDir = 'down'
	elif currDir == 'down':
		currDir = 'left'
	elif currDir == 'left':
		currDir = 'up'
	elif currDir == 'up':
		currDir = 'right'
	else:
		currDir = 'right'
	nextDir = ''
	path = []
	turn = 0
	position = goal
	map = origMap.copy()
	while(position != start):
		print(map)
		print(path)
		print('\n')
		#Add current position to path
		path.append(position)
		prevPosition = position
		#Analyze Surrounding 4 grids
		if(position[0]-1>=0):
			upPos = (position[0]-1,position[1])
		if(position[0]+1<=30):
			doPos = (position[0]+1,position[1])
		if(position[1]-1>=0):
			lePos = (position[0],position[1]-1)
		if(position[1]+1<=16):
			riPos = (position[0],position[1]+1)
		print('Left: ' + str(lePos))
		print('Right: ' + str(riPos))
		print('Up: ' + str(upPos))
		print('Down: ' + str(doPos))
		print('Direction: ' + str(currDir))
		print('Total Turns: ' + str(turn))
		print('Total Points: ' + str(len(path)))
		#Filter out objects
		if map[upPos[0]][upPos[1]] == 0:
			upFlag = 1
		elif map[upPos[0]][upPos[1]] == 1 or map[upPos[0]][upPos[1]] == 8:
			upFlag = 0
		elif map[upPos[0]][upPos[1]] == 3:
			upFlag = 2

		if map[doPos[0]][doPos[1]] == 0:
			doFlag = 1
		elif map[doPos[0]][doPos[1]] == 1 or map[doPos[0]][doPos[1]] == 8:
			doFlag = 0
		elif map[doPos[0]][doPos[1]] == 3:
			doFlag = 2

		if map[lePos[0]][lePos[1]] == 0:
			leFlag = 1
		elif map[lePos[0]][lePos[1]] == 1 or  map[lePos[0]][lePos[1]] == 8:
			leFlag = 0
		elif map[lePos[0]][lePos[1]] == 3:
			leFlag = 2

		if map[riPos[0]][riPos[1]] == 0:
			riFlag = 1
		elif map [riPos[0]][riPos[1]] == 1 or map[riPos[0]][riPos[1]] == 8:
			riFlag = 0
		elif map[riPos[0]][riPos[1]] == 3:
			riFlag = 2
		#Choose a direction
		while(prevPosition == position):
			if riFlag == 2 or leFlag == 2 or doFlag == 2 or upFlag == 2:
				if riFlag == 2:
					position = riPos
				if leFlag == 2:
					position = lePos
				if doFlag == 2:
					position = doPos
				if upFlag == 2:
					position = upPos
			else:
				if currDir == 'up':
					if upFlag == 1:
						position = upPos
					else:
						currDir = 'right'
						turn += 1
				elif currDir == 'right':
					if riFlag == 1:
						position = riPos
					else:
						currDir = 'down'
						turn += 1
				elif currDir == 'down':
					if doFlag == 1:
						position = doPos
					else:
						currDir = 'left'
						turn += 1
				elif currDir == 'left':
					if leFlag == 1:
						position = lePos
					else:
						currDir = 'up'
						turn+=1
				else:
					currDir = 'right'
		map[position[0]][position[1]] = 8
	pathList.append(path)
for i in pathList:
	print(len(i))
