/*
 * generalRobo.h
 *
 *  Created on: Feb 16, 2022
 *      Author: zkingsla
 */


#ifndef GENERALROBO_H_
#define GENERALROBO_H_

void delay(void);
void delay10(void);
void wheelRForward(void);
void wheelRBackward(void);
void wheelLForward(void);
void wheelLBackward(void);
void wheelsForward(void);
void wheelsBackward(void);
void wheelRStop(void);
void wheelLStop(void);
void wheelsStop(void);
void wheelsDemo(void);
void enableWheels(void);
void enablePWM(void);
void enableGPIO(void);
void enableSys(void);
void wheelsRTurn(void);
void wheelsLTurn(void);
void setRPWM(int);
void setLPWM(int);

extern int bumpAIntFlag;
extern int bumpBIntFlag;
#endif GENERALROBO_H_
