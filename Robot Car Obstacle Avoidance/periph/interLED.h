#include <stdint.h>
#include <stdbool.h>
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/pin_map.h"
#include "inc/tm4c123gh6pm.h"
//#include "initpwm.h"
//#include "motor.h"
//#include "initgpio.h"
/*
 * interrupt.h
 *
 *  Created on: Feb 16, 2022
 *      Author: Devin Garcia
 */

#ifndef INTERRUPT_H_
#define INTERRUPT_H_
extern int flagL;
extern int flagR;
void intRWheel(void);
void intLWheel(void);
void enableTimer(void);
void setWheelMatch(int,int);
uint32_t forwardX(int);
uint32_t backwardX(int);
uint32_t turnRX(int);
uint32_t turnLX(int);
void makeRectangle(void);

#endif /* INTERRUPT_H_ */

