#include <stdint.h>
#include <stdbool.h>
#include "driverlib/pwm.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/pin_map.h"
#include "inc/tm4c123gh6pm.h"
#include "interLED.h"
#include "generalRobo.h"
/**
 * lab3.c
 */

void main(void)
{
    enableWheelInt();
    enableAll();

    forwardDist(5);

}
