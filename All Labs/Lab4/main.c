#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "generalRobo.h"
#include "bumper.h"
//#include "positioning.h"

/**
 * main.c
 */
int bumpAIntFlag = 0;
int bumpBIntFlag = 0;
void main()
{
    enableAll();
    bumpAll();
    while(1){
        if(bumpAIntFlag == 1){
            wheelRBackward();
            delay10();
            wheelRStop();
            delay10();
            bumpAIntFlag = 0;
        }
        if(bumpBIntFlag == 1){
            wheelLForward();
            delay10();
            wheelLStop();
            delay10();
            bumpBIntFlag=0;
        }
    }
}
