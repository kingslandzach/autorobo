#include <stdint.h>
#include <stdbool.h>
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/tm4c123gh6pm.h"



void delay(void) {
  for( uint32_t i = 0; i < 250000; ++i );  // requires C99 mode
}
void delay10(void){
    for(int i = 0; i < 10; i++)
        delay();
}
void enableSys(void){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
   while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOB))
       {
       }
   SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
   while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOC))
       {
       }
   SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
   while(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM0))
         {
         }
}
void enableGPIO(void){
    //Set up GPIO Ports B and C
    GPIOPinTypeGPIOOutput(GPIO_PORTC_BASE, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_5 | GPIO_PIN_0 | GPIO_PIN_1);
}
void enablePWM(void){
    //Set PB6 and PB7 to PWM mode
    GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_6 | GPIO_PIN_7);
    //Set clock for PWM (div by 2)
    PWMClockSet(PWM0_BASE,PWM_SYSCLK_DIV_2);
    //Enable PWM Generator 0
    PWMGenEnable(PWM0_BASE, PWM_GEN_0);
    //Configure PWM Generator 0
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);
    //Set PWM Generator Period to 400
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 400);
    //Set PWM0 Output to 25% of 400
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, 200);
    //Set PWM0 Output to 75% of 400
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 200);
    //Configure pins PB6 and PB7 for M0PWM0 and M0PWM1
    GPIOPinConfigure(GPIO_PB6_M0PWM0);
    GPIOPinConfigure(GPIO_PB7_M0PWM1);
    //Set PWM0 and PWM1 to give output
    PWMOutputState(PWM0_BASE, (PWM_OUT_0_BIT | PWM_OUT_1_BIT), true);
}
void enableAll(void){
    enableSys();
    enableGPIO();
    enablePWM();
}
void wheelRForward(void){ //BOTH WHEELS
    //Wheel 1 (RIGHT WHEEL)
    //IN1 = PC5
    //IN2 = PC6
    //PWM = PB7 (from initPWM)
    //STANDBY = PC7
    //Set IN1 = 0 (L); IN2 = STANDBY = 1 (H)
    GPIOPinWrite(GPIO_PORTC_BASE, ( GPIO_PIN_5| GPIO_PIN_6 | GPIO_PIN_7 ) , (GPIO_PIN_6 | GPIO_PIN_7));
}
void wheelLForward(void){
    //Wheel 2 (LEFT WHEEL)
    //IN1 = PB5
    //IN2 = PB0
    //PWM = PB6 (from initPWM)
    //STANDBY = PB1
    //Set IN1 = 0 (L); IN2 = STANDBY = 1 (H)
    GPIOPinWrite(GPIO_PORTB_BASE, (GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5), (GPIO_PIN_0 | GPIO_PIN_1));
}
void wheelsForward(void){
    //Wheel 1 (RIGHT WHEEL)
        //IN1 = PC5
        //IN2 = PC6
        //PWM = PB7 (from initPWM)
        //STANDBY = PC7
        //Set IN1 = 0 (L); IN2 = STANDBY = 1 (H)
        GPIOPinWrite(GPIO_PORTC_BASE, ( GPIO_PIN_5| GPIO_PIN_6 | GPIO_PIN_7 ) , (GPIO_PIN_6 | GPIO_PIN_7));
    //Wheel 2 (LEFT WHEEL)
        //IN1 = PB5
        //IN2 = PB0
        //PWM = PB6 (from initPWM)
        //STANDBY = PB1
        //Set IN1 = 0 (L); IN2 = STANDBY = 1 (H)
        GPIOPinWrite(GPIO_PORTB_BASE, (GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5), (GPIO_PIN_5 | GPIO_PIN_1));
}
void wheelRBackward(void){
    //Wheel 1 (RIGHT WHEEL)
    //IN1 = PC5
    //IN2 = PC6
    //PWM = PB7 (from initPWM)
    //STANDBY = PC7
    //Set IN1 = STANDBY = H; IN2 = L
    GPIOPinWrite(GPIO_PORTC_BASE, ( GPIO_PIN_5| GPIO_PIN_6 | GPIO_PIN_7 ), (GPIO_PIN_5 | GPIO_PIN_7));
}
void wheelLBackward(void){
    //Wheel 2 (LEFT WHEEL)
    //IN1 = PB5
    //IN2 = PB0
    //PWM = PB6 (from initPWM)
    //STANDBY = PB1
    //Set IN1 = STANDBY = H; IN2 = L
     GPIOPinWrite(GPIO_PORTB_BASE, (GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5), (GPIO_PIN_5 | GPIO_PIN_1));
}
void wheelsBackward(void){
    //Wheel 1 (RIGHT WHEEL)
        //IN1 = PC5
        //IN2 = PC6
        //PWM = PB7 (from initPWM)
        //STANDBY = PC7
        //Set IN1 = STANDBY = H; IN2 = L
        GPIOPinWrite(GPIO_PORTC_BASE, ( GPIO_PIN_5| GPIO_PIN_6 | GPIO_PIN_7 ), (GPIO_PIN_5 | GPIO_PIN_7));
    //Wheel 2 (LEFT WHEEL)
        //IN1 = PB5
        //IN2 = PB0
        //PWM = PB6 (from initPWM)
        //STANDBY = PB1
        //Set IN1 = STANDBY = H; IN2 = L
         GPIOPinWrite(GPIO_PORTB_BASE, (GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5), (GPIO_PIN_0 | GPIO_PIN_1));
}
void wheelRStop(void){
    //Wheel 1 (RIGHT WHEEL)
       //IN1 = PC5
       //IN2 = PC6
       //PWM = PB7 (from initPWM)
       //STANDBY = PB7
       //Set IN1 =  IN2 = L; STANDBY = H
       GPIOPinWrite(GPIO_PORTC_BASE, ( GPIO_PIN_5| GPIO_PIN_6 | GPIO_PIN_7 ), (GPIO_PIN_7));
}
void wheelLStop(void){
       //Wheel 2 (LEFT WHEEL)
       //IN1 = PB5
       //IN2 = PB0
       //PWM = PB6 (from initPWM)
       //STANDBY = PB1
       //Set IN1 = IN2 = L; STANDBY = H
        GPIOPinWrite(GPIO_PORTB_BASE, (GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5), (GPIO_PIN_1));
}
void wheelsStop(void){
    //Wheel 1 (RIGHT WHEEL)
       //IN1 = PC5
       //IN2 = PC6
       //PWM = PB7 (from initPWM)
       //STANDBY = PB7
       //Set IN1 =  IN2 = L; STANDBY = H
       GPIOPinWrite(GPIO_PORTC_BASE, ( GPIO_PIN_5| GPIO_PIN_6 | GPIO_PIN_7 ), (GPIO_PIN_7));
       //Wheel 2 (LEFT WHEEL)
       //IN1 = PB5
       //IN2 = PB0
       //PWM = PB6 (from initPWM)
       //STANDBY = PB1
       //Set IN1 = IN2 = L; STANDBY = H
        GPIOPinWrite(GPIO_PORTB_BASE, (GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_5), (GPIO_PIN_1));
}
void wheelsRTurn(void){
    wheelRBackward();
    wheelLBackward();
}
void wheelsLTurn(void){
    wheelRForward();
    wheelLForward();
}
void setRPWM(float pwm0){
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, (pwm0*4));
}
void setLPWM(float pwm1){
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, (pwm1*4));
}
void wheelsDemo(void){
   enableAll();
   wheelsForward();
   delay10();
   wheelsBackward();
   delay10();
   wheelRForward();
   wheelLBackward();
   delay10();
   wheelRBackward();
   wheelLForward();
   delay10();
   wheelRBackward();
   setRPWM(100);
   delay10();
   wheelRForward();
   wheelLForward();
   setLPWM(100);
   delay10();
   wheelRStop();
   wheelLStop();
}







